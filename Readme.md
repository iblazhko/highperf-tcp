High-Performance TPC/IP Library
===============================

Version 1.0
03/12/2014


Introduction
------------

Asynchronous TCP/IP library that offers high-performance server and client implementations
as well as packet-based serial communication protocol.